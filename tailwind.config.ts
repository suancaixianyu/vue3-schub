module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {},
  },
  plugins: [],
}

export const daisyui = {
  themes: ['light', 'dark'],
}
export const plugins = [require('daisyui')]
