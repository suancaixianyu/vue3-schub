import { reactive } from 'vue'
import type { userInfoType, setType } from '@/apitypes'

/** 内容样式 */
const maincontainer = reactive({
  padding: '0',
  height: 'calc(100vh - 4rem)',
  overflowY: 'auto',
})

/** 卡片样式 */
const postliststyle = reactive({
  display: 'flex',
  padding: '12px 18px',
  justifyContent: 'center',
  margin: '10px auto',
  width: '95%',
  maxHeight: '200px',
  boxShadow: 'var(--el-box-shadow-light)',
  borderRadius: 'var(--rounded-btn)',
})
/** 页面整体样式 */
const container = reactive({
  padding: '0',
  margin: '0',
})
const headsize = reactive({
  post: 28,
  userindex: 48,
})

const set = reactive({
  menu: true,
  shape: 'square',
  ismobile: false,
  showheader: false,
  topbtn: false,
  topmenu: false,
  showfooter: true,
  background: 'https://pic.imgdb.cn/item/64e8cf5d661c6c8e5411e646.png',
})

let webstyle = reactive({
  webkit: {
    // 滚动条样式
    'scrollbar-thumb-background': '',
    'scrollbar-thumb-background-color': '#F2F2F2',
    'scrollbar-thumb-border': 'none',
    'scrollbar-thumb-border-radius': '10px',
    'scrollbar-thumb-background-size': '100%',
    'scrollbar-thumb-background-hover': 'var(--scrollbar-thumb-background)',
    'scrollbar-thumb-background-color-hover': '#BDBDBD',
    'scrollbar-thumb-border-hover': 'none',
    'scrollbar-thumb-border-radius-hover': '10px',
    'scrollbar-thumb-background-size-hover': '100%',
    'scrollbar-width': '8px',
  },
  card: {
    'rounded-card': 'var(--rounded-btn)',
  },
})

const userInfo: userInfoType = reactive({
  global_mod_data_list: {
    game_version: [],
    api_version: [],
    relate_type: [],
    link_type: [],
    flag_list: [],
    server_version_list: [],
  },
  role_list: [],
  cate_list: [],
  state: {
    unreadMessage: 0,
    isLogin: false,
    isLoginDialogVisible: false,
  },
  data: {
    role: '',
    nickname: '',
    headurl: '',
    id: 0,
    signature: '',
    isAdmin: false,
    token: ''
  },
})

const webpublic = reactive({
  active_cate_id: 0,
})

const postListScrollPosition = reactive({ top: 0, left: 0 })
let postcontainer = reactive({ el: null })

class Cfg {
  config: {
    /** 后端服务器地址 */
    server: string
    /** 下载节点服务器 **/
    down_server: string
    /** 上传节点服务器 **/
    upload_server: string
    /** 前端地址 */
    view: string
  }
  /** 全局样式 */
  webstyle: { [key: string]: { [key: string]: string } }
  /** 用户信息 */
  userInfo: userInfoType
  /** 设置 */
  set: setType

  /** 用户头像大小 */
  headsize: {
    /** 帖子显示大小 */
    post: number
    /** 个人中心显示大小 */
    userindex: number
  }
  container: {
    padding: string
    margin: string
  }
  maincontainer: { [key: string]: string }
  postliststyle: { [key: string]: string }
  /** 网页其他缓存 */
  webpublic: {
    /** 当前编辑的帖子所在的板块id */
    active_cate_id: number
  }
  /** 帖子列表滚动状态 */
  postListScrollPosition: { top: number; left: number }
  postcontainer: { el: HTMLElement | null }

  constructor() {
    this.config = {
      /** 后端服务器地址 */
      server: 'https://m.schub.top',
      /**下载**/
      down_server: 'https://down.schub.top',
      /**上传**/
      upload_server: 'https://m.schub.top',
      view: 'https://www.schub.top'
    }
    this.webstyle = webstyle
    this.userInfo = userInfo
    this.set = set
    this.headsize = headsize
    this.container = container
    this.postliststyle = postliststyle
    this.maincontainer = maincontainer
    this.webpublic = webpublic
    this.postListScrollPosition = postListScrollPosition
    this.postcontainer = postcontainer
  }
}

export default new Cfg()
