export interface roleItem {
  id: number
  /** 头衔名称 **/
  name: string
  stat: number
  /** 参考el-tag的color属性 **/
  color: string
  /** 参考el-tag的type属性 **/
  type: string
  /** 参考el-tag的effect属性 **/
  effect: string
}
