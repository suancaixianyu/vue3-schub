export interface bbsItem {
  bad_num: number
  cate_id: number
  comment_num: number
  content: string
  cover_src: string
  create_time: number
  create_time_str: string
  good_num: number
  id: number
  last_edit_time: number
  reply_num: number
  stat: number
  title: string
  top: number
  top_time: number
  uid: number
  view_num: number
}
