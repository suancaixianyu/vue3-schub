export interface modItem {
  /** 资源id */
  id: number
  /** 用户uid */
  uid: number
  /** 服务器id */
  sd_id?: number
  /** 资源名称 */
  name: string
  /** 资源简介 */
  description: string
  /** 资源包名 */
  package_id: string
  /** 资源封面 */
  cover_src: string
  /** 发布时间 */
  create_time: number | string
  /** 最后修改时间 */
  last_modify_time: number | string
  /** 浏览量 */
  views: number | string
  /** 资源状态 */
  stat: number
  /** 点赞数 */
  likes: number | string
  /** 下载次数 */
  downloads: number | string
  /** 英文名称 */
  en_name: string
  /** 简称 */
  mini_name: string
  rate: number
  /** 封禁原因 */
  reason: null
  create_time_str: string
  flag_list: string
  flag_list_arr: string[]
  stat_data: {
    /** 状态名称 */
    name: string
    /** 状态类型 */
    type: string
  }
}

export interface serverItem {
  /** 服务器id */
  sd_id: number
  /** 归属modid */
  mod_id: number
  /** 服务器名称 */
  name: string
  /** 服务器ip */
  ip: string
  /** 服务器版本 */
  version: string
  /** 服务器状态 */
  stat: number
  /** 服务器等级 */
  level: number
  /** 描述 */
  description: string
  /** 创建时间 */
  time: number | string
  /** 用户名称 */
  uname: string
  /** 用户uid */
  uid: number
  stat_data: stat_data
}
export interface stat_data {
  name: string
  type: 'success' | 'info' | 'warning' | 'danger' | ''
}
