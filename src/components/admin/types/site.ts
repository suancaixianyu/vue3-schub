export interface siteConfig {
  id: number
  name: string
  value: string
  comments: string
}
export interface siteSaveConfig {
  id: number
  value: string
}
export interface serverInfo {
  member: number
  bbs: number
  mod: number
}
