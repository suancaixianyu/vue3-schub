export interface memberItem {
  id: number
  account: string
  head_img: string
  signature: string
  background: string
  nickname: string
  reg_time: number
  last_login_time: number
  last_login_ip: string
  money: number
  stat: number
  email: string
  role_list: string
  remark: string
}
