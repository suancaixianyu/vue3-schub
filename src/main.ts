import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

/** daisy样式库 */
import 'tailwindcss/tailwind.css'
import 'daisyui/dist/full.css'

/** element样式库 */
import 'element-plus/theme-chalk/display.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

/** md编辑器 */
import 'md-editor-v3/lib/style.css'
import 'md-editor-v3/lib/preview.css'

/** 自定义样式，用于对预设样式覆盖 */
// import './output.css'
import './style.css'

/** 自定义全局方法 */
import Method from './globalmethods'

const app = createApp(App)

// 导入各种插件
app.use(ElementPlus)
app.use(router)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

// 初始化页面样式
Method.setwebstyle()

// 指定容器
app.mount('#app')
