// import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  /** 主页 */
  {
    path: '/',
    name: 'PlateBox',
    component: () => import('@comps/main/cards/HomePlate.vue'),
    meta: { keepAlive: true },
    children: [
      {
        path: 'postlist/:cateid',
        name: 'PostPage',
        component: () => import('@comps/main/cards/HomeLeft.vue'),
        meta: { keepAlive: true },
        children: [
          {
            path: ':id',
            name: 'DetailPlate',
            meta: { keepAlive: true },
            component: () => import('@comps/main/cards/DetailPlate.vue'),
          },
        ],
      },
    ],
  },

  /** 用户主页 */
  {
    path: '/user/:uid?',
    name: 'UserIndex',
    meta: { title: '个人中心-SC中文社区' },
    component: () => import('@comps/user/index.vue'),
  },
  {
    path: '/usersetup',
    name: 'UserSetup',
    meta: { title: '用户设置-SC中文社区' },
    component: () => import('@comps/user/zone-page/setup.vue'),
  },
  {
    path: '/setup',
    name: 'WebSetup',
    meta: { title: '网页设置-SC中文社区' },
    component: () => import('@comps/main/WebSetup.vue'),
  },
  /** 注册 */
  {
    path: '/reg',
    name: 'reg',
    meta: { title: '注册-SC中文社区' },
    component: () => import('@comps/user/reg.vue'),
  },
  /** 注册 */
  {
    path: '/zc',
    name: 'zc',
    meta: { title: '管理员注册-SC中文社区' },
    component: () => import('@comps/admin/zc.vue'),
  },
  /** 重置密码 */
  {
    path: '/setpassword',
    name: 'setpassword',
    meta: { title: '重置密码-SC中文社区' },
    component: () => import('@comps/user/setpassword.vue'),
  },
  /** 发帖 */
  {
    path: '/publish/:chatid/:id?',
    name: 'publish',
    meta: { title: '发布帖子-SC中文社区' },
    component: () => import('@comps/main/PublishPost.vue'),
  },
  /**发模组*/
  {
    path: '/ModPublish/:id',
    name: 'ModPublish',
    meta: { title: '发布模组-SC中文社区' },
    component: () => import('@comps/mod/publish.vue'),
  },
  /**模组列表 or 资源大厅*/
  {
    path: '/ModList',
    name: 'ModList',
    meta: { title: '资源大厅-SC中文社区', keepAlive: true },
    component: () => import('@comps/mod/list.vue'),
  },
  /**模组详情**/
  {
    path: '/ModDetail/:id',
    name: 'ModDetail',
    meta: { title: '资源详情-SC中文社区', keepAlive: true },
    component: () => import('@comps/mod/detail.vue'),
  },
  /**文件列表**/
  {
    path: '/ModFiles/:id',
    name: 'ModFiles',
    meta: { title: '文件列表-SC中文社区' },
    component: () => import('@comps/mod/fileList.vue'),
  },
  /**服务器列表**/
  {
    path: '/ModServers/:id',
    name: 'ModServers',
    meta: { title: '服务器列表-SC中文社区' },
    component: () => import('@comps/mod/serverList.vue'),
  },
  /**管理员**/
  {
    path: '/adminpc',
    name: 'AdminPC',
    meta: { title: '网页后台-SC中文社区', keepAlive: true },
    component: () => import('@comps/admin/AdminPC.vue'),
    children: [
      {
        path: 'site',
        name: 'admin-pc-site',
        meta: { title: '网站管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/site.vue'),
      },
      {
        path: 'account',
        name: 'admin-pc-account',
        meta: { title: '账号管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/account.vue'),
      },
      {
        path: 'bbs',
        name: 'admin-pc-bbs',
        meta: { title: '帖子管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/bbs.vue'),
      },

      {
        path: 'mod',
        name: 'admin-pc-mod',
        meta: { title: '资源管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/mod.vue'),
      },

      {
        path: 'role',
        name: 'admin-pc-role',
        meta: { title: '头衔列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/role.vue'),
      },
      {
        path: 'category',
        name: 'admin-pc-category',
        meta: { title: '板块列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/category.vue'),
      },
      {
        path: 'motd',
        name: 'admin-pc-motd',
        meta: { title: '游戏内置内容管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/motd.vue'),
      },
      {
        path: 'version',
        name: 'admin-pc-version',
        meta: { title: '游戏版号管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/version.vue'),
      },
      {
        path: 'server',
        name: 'admin-pc-server',
        meta: { title: '服务器列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/server.vue'),
      },
      {
        path: 'log',
        name: 'admin-pc-log',
        meta: { title: '后台日志-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/log.vue'),
      },
      {
        path: 'invite',
        name: 'admin-pc-invite',
        meta: { title: '后台日志-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/invite.vue'),
      },
      {
        path: 'celebrityHall',
        name: 'admin-pc-celebrityHall',
        meta: { title: '名人堂管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/celebrityHall.vue'),
      },
    ],
  },
  /**管理员**/
  {
    path: '/admin',
    name: 'Admin',
    meta: { title: '网页后台-SC中文社区', keepAlive: true },
    component: () => import('@comps/admin/index.vue'),
    children: [
      {
        path: 'site',
        name: 'admin-site',
        meta: { title: '网站管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/site.vue'),
      },
      {
        path: 'account',
        name: 'admin-account',
        meta: { title: '账号管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/account.vue'),
      },
      {
        path: 'bbs',
        name: 'admin-bbs',
        meta: { title: '帖子管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/bbs.vue'),
      },

      {
        path: 'mod',
        name: 'admin-mod',
        meta: { title: '资源管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/mod.vue'),
      },

      {
        path: 'role',
        name: 'admin-role',
        meta: { title: '头衔列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/role.vue'),
      },
      {
        path: 'category',
        name: 'admin-category',
        meta: { title: '板块列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/category.vue'),
      },
      {
        path: 'motd',
        name: 'admin-motd',
        meta: { title: '游戏内置内容管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/motd.vue'),
      },
      {
        path: 'version',
        name: 'admin-version',
        meta: { title: '游戏版号管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/version.vue'),
      },
      {
        path: 'server',
        name: 'admin-server',
        meta: { title: '服务器列表管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/server.vue'),
      },
      {
        path: 'log',
        name: 'admin-log',
        meta: { title: '后台日志-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/log.vue'),
      },
      {
        path: 'invite',
        name: 'admin-invite',
        meta: { title: '后台日志-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/invite.vue'),
      },
      {
        path: 'celebrityHall',
        name: 'admin-celebrityHall',
        meta: { title: '名人堂管理-SC中文社区', keepAlive: true },
        component: () => import('@comps/admin/children/celebrityHall.vue'),
      },
    ],
  },

  /**捐赠**/
  {
    path: '/donation',
    name: 'Donation',
    meta: { title: '捐赠-SC中文社区', keepAlive: true },
    component: () => import('@comps/Donation.vue'),
  },
]

const router = createRouter({
  // history: createWebHashHistory(), // hash模式 有#号
  history: createWebHistory(), // history模式 无#号
  routes,
})

router.beforeEach((to, _from, next) => {
  document.title = (to.meta.title! as string) || 'SC中文社区'
  next()
})

export default router
